package de.fhbingen.epro.vl4;

import de.fhbingen.epro.vl4.exception.CalculatorException;

public class Calculator {
	
	private CrazyMultiplier crazyMultiplier;
	
	public void setCrazyMultiplier(CrazyMultiplier crazyMultiplier) {
		this.crazyMultiplier = crazyMultiplier;
	}

	public float add(float x, float y) throws CalculatorException {
		return x + y;
	}
	
	public float sub(float x, float y) throws CalculatorException {
		return x - y;
	}
	
	public float multiply(float x, float y) throws CalculatorException {
		return crazyMultiplier.multiply(x, y);
	}

}
