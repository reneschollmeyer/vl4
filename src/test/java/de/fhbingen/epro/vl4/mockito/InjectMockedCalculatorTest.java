package de.fhbingen.epro.vl4.mockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import de.fhbingen.epro.vl4.Calculator;
import de.fhbingen.epro.vl4.CrazyMultiplier;
import de.fhbingen.epro.vl4.exception.CalculatorException;

@RunWith(MockitoJUnitRunner.class)
public class InjectMockedCalculatorTest {
	
	@Mock
	private CrazyMultiplier crazyMultiplier;
	
	@InjectMocks
	private Calculator calculator;

	@Test
	public void shouldInjectMocks() throws CalculatorException {
		
		calculator.multiply(1f, 1f);
		
		Mockito.verify(crazyMultiplier, Mockito.atLeastOnce()).multiply(1f, 1f);
	}

}
