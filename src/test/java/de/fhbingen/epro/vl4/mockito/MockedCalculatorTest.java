package de.fhbingen.epro.vl4.mockito;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import de.fhbingen.epro.vl4.Calculator;
import de.fhbingen.epro.vl4.exception.CalculatorException;

public class MockedCalculatorTest {

	@Test
	public void shouldMockWrongResult() throws CalculatorException  {
	  Calculator calculator = Mockito.mock(Calculator.class);
	  
	  Mockito.when(calculator.add(10f, 11f)).thenReturn(211f);
	  
	  assertEquals(calculator.add(10f, 11f), 211, 0);
	}
	
	@Test(expected=CalculatorException.class)
	public void shouldMockCalculatorException() throws CalculatorException {
	  Calculator calculatorMock = Mockito.mock(Calculator.class);
	  Mockito.doThrow(new CalculatorException()).when(calculatorMock).add(1f, 1000000f);
	  
	  Calculator calculator = calculatorMock;
	  calculator.add(1f, 1000000f);
	} 
	
	@Test
	public void shouldVerifyNumberOfCalls() throws CalculatorException {
		Calculator calculator = Mockito.mock(Calculator.class);
		Mockito.when(calculator.add(10f, 11f)).thenReturn(211f);
		
		calculator.add(1f, 2000f);
		calculator.add(1f, 2000f);
		calculator.add(1f, 2000f);
		calculator.add(1f, 20020f);
		
		Mockito.verify(calculator, Mockito.times(3)).add(1f, 2000f);
		
		Mockito.verify(calculator, Mockito.atLeastOnce()).add(1f, 2000f);
		
		Mockito.verify(calculator, Mockito.never()).sub(1f, 2000f);
	}
	
	@Test
	public void shouldSpyOnList() throws CalculatorException {
		List<String> list = new LinkedList<String>();
		List<String> spy = Mockito.spy(list);

		Mockito.doReturn("foo").when(spy).get(0);

		Mockito.when(spy.get(0)).thenReturn("foo"); 
	}

}
